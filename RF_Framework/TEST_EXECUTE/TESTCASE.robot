*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    https://demo.abantecart.com/    chrome
    [W] Capture Page Screenshot
    [W] Click Element    //a[contains(.,'Login or register')]
    [W] Capture Page Screenshot
    [W] Input Text    //input[@id='loginFrm_loginname']    @{ListData}[0]
    [W] Capture Page Screenshot
    [W] Input Text    //input[@name='password']    @{ListData}[1]
    [W] Capture Page Screenshot
    [W] Click Element    //button[contains(.,'Login')]
    [W] Capture Page Screenshot
    [W] Click Element    (//a[contains(.,'Makeup')])[2]
    [W] Capture Page Screenshot
    [W] Click Element    (//a[contains(.,'Lips')])[2]
    [W] Capture Page Screenshot
    [W] Click Element    (//a[@class='prdocutname'])[3]
    [W] Capture Page Screenshot
    [W] Click Element    //a[@class='cart']
    [W] Capture Page Screenshot
    [W] Click Element    //select[@name='shippings']//option[2]
    [W] Capture Page Screenshot
    [W] Click Element    (//i[@class='fa fa-shopping-cart fa-fw'])[5]
    [W] Capture Page Screenshot
    [W] Click Element    (//div[@class='checkbox_place'])[2]
    [W] Capture Page Screenshot
    [W] Click Element    //button[contains(.,'Confirm Order')]
    [W] Capture Page Screenshot
    [W] Check Meaasge    Order is completed!
    [W] Capture Page Screenshot
    [W] Click Element    //a[contains(@class,'parent')]
    [W] Capture Page Screenshot
    [W] Click Element    (//a[contains(.,'Account')])[4]
    [W] Capture Page Screenshot
    [W] Click Element    (//a[contains(.,'Logoff')])[2]
    [W] Capture Page Screenshot
    [W] Click Element    //a[@class='btn btn-default mr10']
